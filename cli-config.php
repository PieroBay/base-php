<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

require_once 'vendor/autoload.php';

$settings = include 'config/settings.php';
$doctrine = $settings['doctrine'];

$config = Setup::createAnnotationMetadataConfiguration(
    [$settings['root'] . '/src/Entity'],
    $doctrine['dev_mode'],
    $settings['root'] . '/var/proxies',
    null,
    false
);
$em = EntityManager::create($doctrine['connection'], $config);
return ConsoleRunner::createHelperSet($em);
