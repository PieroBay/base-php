const path = require('path');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const {WebpackManifestPlugin} = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const glob = require("glob");
const { VueLoaderPlugin } = require('vue-loader');

/**
 * Crawl les fichiers et crée un objet
 * nom => chemin
 * nom est le chemin du fichier final
 * 
 * @param {array<string>} paths 
 * @returns {object}
 */
function dynamicFiles(paths) {
    let files = {}
    for (let path of paths) {
        glob.sync(path).filter(entry => {
            var key = entry
                .replace('./src/Controllers/', '')
                .replace('/inc/', '/')
                .replace(/scss/g, 'css')
                .replace('.css', '')
                .toLocaleLowerCase();
            files[key] = entry;
        })
    }
    return files
}

module.exports = {
    entry: dynamicFiles(["./src/Controllers/*/inc/*(js|scss)/*.*(js|scss)"]),
    output: {
        path: path.resolve(__dirname, "./public/assets/"),
        filename: "[name]"
    },
    optimization: {
        minimizer: [new TerserJSPlugin({}), new CssMinimizerPlugin({})],
    },
    performance: {
        maxEntrypointSize: 1024000,
        maxAssetSize: 1024000
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
        {
            test: /\.scss$/i,
            use: [
                MiniCssExtractPlugin.loader,
                "css-loader",
                "sass-loader"
            ],
        },
        {
            test: /\.vue$/,
            loader: 'vue-loader'
        },
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new WebpackManifestPlugin(),
        new MiniCssExtractPlugin({
            ignoreOrder: false
        }),
        new VueLoaderPlugin()
    ],
    watchOptions: {
        ignored: ['./node_modules/']
    },
    mode: "development"
};