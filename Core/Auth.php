<?php

namespace App\Core;

use AppSrc\Entity\User;
use AppSrc\Enum\UserRoleEnum;

class Auth {
    public static function login(User $user): User {
        $_SESSION['user'] = [
            'id' => $user->getId(),
            'role' => UserRoleEnum::from($user->getRole()->getId())
        ];

        return $user;
    }

    public static function logout(): void {
        $_SESSION['user'] = [
            'id' => null,
            'role' => UserRoleEnum::GUEST
        ];
    }

    public static function hashPassword(string $password): string {
        return hash('sha512', md5($password));
    }

    public static function isLoggedIn(): bool {
        return $_SESSION['user']['role'] != UserRoleEnum::GUEST;
    }

    public static function isAuthorized(UserRoleEnum $roleRequired): bool {
        $currentRole = isset($_SESSION['user']['role']) ? $_SESSION['user']['role'] : UserRoleEnum::GUEST;
        return in_array($roleRequired, $currentRole->roleToGroup());
    }
}
