<?php

namespace App\Core;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Slim\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RouteContext;
use Twig\Environment;

class BaseController {

    protected Environment $twig;
    protected ContainerInterface $c;
    protected EntityManager $em;

    public function __construct(ContainerInterface $c, EntityManager $em, Environment $twig) {
        $this->twig = $twig;
        $this->c = $c;
        $this->em = $em;
    }

    protected function render(array $args, ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();

        $response->getBody()->write(
            $this->twig->render(
                $this->getChildClassName() . '/templates/' . debug_backtrace()[1]['function'] . '.twig',
                array_merge(
                    $args,
                    [
                        'assets' => [
                            'root' => '/' . $this->c->get('settings')['subFolder'] . 'assets/',
                            'js' => '/' . $this->c->get('settings')['subFolder'] . 'assets/' . $route->getName() . '/js/',
                            'pageJs' => '<script src="/' . $this->c->get('settings')['subFolder'] . 'assets/' . $route->getName() . '/js/' . $route->getName() . '.js"></script>',
                            'css' => '/' . $this->c->get('settings')['subFolder'] . 'assets/' . $route->getName() . '/css/',
                            'pageCss' => '<link rel="stylesheet" href="/' . $this->c->get('settings')['subFolder'] . 'assets/' . $route->getName() . '/css/' . $route->getName() . '.css" />',
                        ],
                        'lang' => (isset($request)) ? $request->getAttribute('lang') : $_SESSION['lang'],
                        'route' => $routeContext->getRoute()
                    ]
                )
            )
        );
        return $response;
    }

    private function getChildClassName(): string {
        $class_parts = explode('\\', get_class($this));
        $reformateFileName = strtolower(str_replace('Controller', '', end($class_parts)));
        return $reformateFileName;
    }
}
