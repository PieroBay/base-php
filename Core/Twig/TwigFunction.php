<?php

namespace App\Core\Twig;

class TwigFunction {
    private string $name;
    private TwigTypeEnum $type;

    public function __construct(string $name, TwigTypeEnum $type) {
        $this->name = $name;
        $this->type = $type;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getType(): TwigTypeEnum {
        return $this->type;
    }
}