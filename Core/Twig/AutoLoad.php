<?php

namespace App\Core\Twig;

use App\Core\Twig\Custom\InArrayFilter;
use App\Core\Twig\Custom\SlugifyFilter;
use App\Core\Twig\Custom\PathFunction;
use App\Core\Twig\TwigTypeEnum;
use Psr\Container\ContainerInterface;
use Twig;

class AutoLoad {
    private Twig\Environment $twig;
    private ContainerInterface $container;

    public function __construct(Twig\Environment $twig, ContainerInterface $container) {
        $this->twig = $twig;
        $this->container = $container;
    }

    public function addCustomFunctions(): Twig\Environment {
        foreach ($this->_getAllFunctions() as $value) {
            if ($value->getType() === TwigTypeEnum::FILTER) {
                $filter = new Twig\TwigFilter($value->getName(), $value);
                $this->twig->addFilter($filter);
            } elseif ($value->getType() === TwigTypeEnum::FUNCTION) {
                $function = new Twig\TwigFunction($value->getName(), $value);
                $this->twig->addFunction($function);
            }
        }
        return $this->twig;
    }

    private function _getAllFunctions() {
        return [
            new SlugifyFilter(),
            new PathFunction($this->container),
            new InArrayFilter(),
        ];
    }
}