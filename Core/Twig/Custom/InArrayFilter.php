<?php
namespace App\Core\Twig\Custom;

use App\Core\Twig\TwigFunction;
use App\Core\Twig\TwigTypeEnum;

/**
 * Find if value exist in simple or multi-dimensionnel array
 */
class InArrayFilter extends TwigFunction {
    public function __construct() {
        parent::__construct('inArray', TwigTypeEnum::FILTER);
    }

    public function __invoke(array $array, string $key, string $value, bool $isMulti = true): bool {
        if (!$isMulti) {
            return in_array($value, $array);
        } else {
            foreach ($array as $k => $v) {
                if ($v[$key] === $value) {
                    return true;
                }
            }
            return false;
        }
    }
}
