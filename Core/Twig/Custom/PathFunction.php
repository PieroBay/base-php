<?php

namespace App\Core\Twig\Custom;

use App\Core\Twig\TwigFunction;
use App\Core\Twig\TwigTypeEnum;
use Psr\Container\ContainerInterface;
use Slim\Interfaces\RouteParserInterface;

/**
 * Return uri from route name
 */
class PathFunction extends TwigFunction {
    private RouteParserInterface $routeParser;

    public function __construct(ContainerInterface $container) {
        parent::__construct('path', TwigTypeEnum::FUNCTION);
        $this->routeParser = $container->get(RouteParserInterface::class);
    }

    public function __invoke(string $routeName, ?array $params = []): string {
        if (!isset($params['lang'])) {
            $params['lang'] = $_SESSION['lang'];
        }
        return $this->routeParser->relativeUrlFor($routeName, $params);
    }
}
