<?php

namespace App\Core\Twig\Custom;

use App\Core\Twig\TwigFunction;
use App\Core\Twig\TwigTypeEnum;

/**
 * Return slugified string
 */
class SlugifyFilter extends TwigFunction {
    public function __construct() {
        parent::__construct('slugify', TwigTypeEnum::FILTER);
    }

    public function __invoke(string $str): string {
        // Convert to lowercase and remove whitespace
        $str = mb_strtolower(trim($str));
        // Replace high ascii characters
        $pattern = array("/(é|è|ë|ê)/", "/(ó|ò|ö|ô)/", "/(ú|ù|ü|û)/", "/(à|á|ä|â)/", "/(î|ï|ì|í)/", "/(ç)/", "/('|&)/");
        $replacements = array("e", "o", "u", "a", "i", "c", "-");
        $str = preg_replace($pattern, $replacements, $str);
        // Remove puncuation
        $pattern = array(":", "!", "?", ".", "/", '"', "(", ")", "[", "]");
        $str = str_replace($pattern, "", $str);
        // Hyphenate any non alphanumeric characters
        $pattern = array("/[^a-z0-9-]/", "/-+/");
        $str = preg_replace($pattern, "-", $str);
        return $str;
    }
}
