<?php

namespace App\Core\Twig;

enum TwigTypeEnum: string {
    case FUNCTION = 'FUNCTION';
    case FILTER = 'FILTER';
}