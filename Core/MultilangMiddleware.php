<?php

namespace App\Core;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Slim\App;

class MultilangMiddleware implements MiddlewareInterface {

    private App $app;
    private array $settings;

    function __construct(App $app) {
        $this->app = $app;
        $this->settings = $app->getContainer()->get('settings');
        $this->container['language'] = $this->settings['langs']['default'];
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        // Remove subfolder from path if exist
        $path = str_replace($this->settings['subFolder'], '', $request->getUri()->getPath());
        // explode url by slash
        $getLangInUri = array_values(array_filter(explode("/", $path)));
        // if value in array get first
        $getLangInUri = $getLangInUri[0] ?? '';

        // Check if only 2 chars and if is in available languages
        if (strlen($getLangInUri) == 2 && in_array($getLangInUri, $this->settings['langs']['available'])) {
            $this->_setLangInApp($request, $getLangInUri);
            return $handler->handle($request);
            // if no lang in url, check if it's from excluded route and accept it
        } else if (in_array($getLangInUri, $this->settings['langs']['exclude_route'])) {
            $this->_getBrowserLanguageAndSetToApp($request);
            return $handler->handle($request);
        } else { // otherwise add lang in url and redirect
            $newUri = $this->_getBrowserLanguageAndSetToApp($request) . rtrim($path, "/");
            return $this->app->getResponseFactory()->createResponse()->withHeader('Location', $newUri)->withStatus(302);
        }
    }

    private function _getBrowserLanguageAndSetToApp(ServerRequestInterface $request): string {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $langToReturn = $lang;
        if (isset($_SESSION['lang']) && in_array($_SESSION['lang'], $this->settings['langs']['available'])) {
            $langToReturn = $_SESSION['lang'];
        } else if (in_array($lang, $this->settings['langs']['available'])) {
            $langToReturn = $lang;
        } else {
            $langToReturn = $this->settings['langs']['default'];
        }
        $this->_setLangInApp($request, $langToReturn);
        return $langToReturn;
    }

    private function _setLangInApp(ServerRequestInterface $request, string $lang): void {
        $request->withAttribute('lang', $lang);
        $_SESSION['lang'] = $lang;
    }
}
