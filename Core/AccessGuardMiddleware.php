<?php

namespace App\Core;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use AppSrc\Enum\UserRoleEnum;
use App\Core\Auth;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

class AccessGuardMiddleware {
    private RouteCollectorProxy $app;
    private UserRoleEnum $roleRequired;
    private array $settings;

    public function __construct(RouteCollectorProxy $app, UserRoleEnum $roleRequired) {
        $this->app = $app;
        $this->settings = $this->app->getContainer()->get('settings');
        $this->roleRequired = $roleRequired;
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler) {
        if (!Auth::isAuthorized($this->roleRequired)) {
            return $this->app->getResponseFactory()->createResponse()->withHeader('Location', '/' . $this->settings['subFolder'])->withStatus(302);
        }
        return $handler->handle($request);
    }
}
