<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Timezone
date_default_timezone_set('Europe/Brussels');

// Settings
$settings = [
    'mode' => 'dev',
    'root' => dirname(__DIR__),
    'subFolder' => 'Git/base-php/'
];

$settings['langs'] = [
    "exclude_route" => ["api"],
    "default" => "fr",
    "available" => ["en", "fr"]
];

// Error Handling Middleware settings
$settings['error'] = [
    'display_error_details' => ($settings['mode'] === 'dev'),
    'log_errors' => true,
    'log_error_details' => true,
];

// Database settings
$settings['db'] = [
    'driver' => 'pdo_mysql',
    'host' => 'localhost',
    'username' => 'root',
    'database' => 'test',
    'password' => '',
    'port' => 3306,
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'flags' => [
        // Turn off persistent connections
        PDO::ATTR_PERSISTENT => false,
        // Enable exceptions
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        // Emulate prepared statements
        PDO::ATTR_EMULATE_PREPARES => true,
        // Set default fetch mode to array
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        // Set character set
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
    ]
];

$settings['doctrine'] = [
    // if true, metadata caching is forcefully disabled
    'dev_mode' => ($settings['mode'] === 'dev'),
    // path where the compiled metadata info will be cached
    // make sure the path exists and it is writable
    'cache_dir' => $settings['root'] . '/var/doctrine',
    // you should add any other path containing annotated entity classes
    'metadata_dirs' => [$settings['root'] . '/src/Models'],
    'proxy_dir' => [$settings['root'] . '/var/proxy'],
    'connection' => [
        'driver' => $settings['db']['driver'],
        'host' => $settings['db']['host'],
        'port' => $settings['db']['port'],
        'dbname' => $settings['db']['database'],
        'user' => $settings['db']['username'],
        'password' => $settings['db']['password'],
        'charset' => $settings['db']['charset'],
    ]
];

return $settings;
