<?php

use App\Core\MultilangMiddleware;
use Slim\App;
use Selective\BasePath\BasePathMiddleware;
use Slim\Middleware\ErrorMiddleware;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    // remove trailing slashes
    $app->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) use ($app) {
        $response = $app->getResponseFactory()->createResponse();
        $uri = $request->getUri();
        $path = $uri->getPath();

        if ($path != '/' && substr($path, -1) == '/') {
            $uri = $uri->withPath(substr($path, 0, -1));

            if ($request->getMethod() == 'GET') {
                return $response->withHeader('Location', $uri->getPath())->withStatus(302);
            } else {
                return $handler->handle($request->withUri($uri), $response);
            }
        }
        return $handler->handle($request);
    });

    $app->add(BasePathMiddleware::class);

    $app->add(MultilangMiddleware::class);

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);
};
