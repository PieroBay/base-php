<?php

use Slim\App;
use AppSrc\Controllers\Home\HomeRouting;
use AppSrc\Controllers\Api\ApiRouting;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->group('/{lang:[a-z]{2}}', function (RouteCollectorProxy $group) {
        new HomeRouting($group);
    });

    new ApiRouting($app);
};
