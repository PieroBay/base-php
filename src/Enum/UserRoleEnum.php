<?php

namespace AppSrc\Enum;

enum UserRoleEnum: int {
    case GUEST  = 1;
    case USER   = 2;
    case ADMIN  = 3;

    public function roleToGroup(): array {
        return match ($this) {
            UserRoleEnum::GUEST => [UserRoleEnum::GUEST],
            UserRoleEnum::USER => [UserRoleEnum::USER, UserRoleEnum::GUEST],
            UserRoleEnum::ADMIN => [UserRoleEnum::ADMIN, UserRoleEnum::USER, UserRoleEnum::GUEST]
        };
    }
}
