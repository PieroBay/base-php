<?php

namespace AppSrc\Service;

use App\Core\Auth;
use AppSrc\Entity\User;
use AppSrc\Entity\Role;
use Doctrine\ORM\EntityManager;
use AppSrc\Enum\UserRoleEnum;

final class UserService {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function signUp(array $user): User {
        $userEntiy = new User();
        $userEntiy->setName($user['name']);
        $userEntiy->setUsername($user['username']);
        $userEntiy->setPassword($user['password']);
        $userEntiy->setEmail($user['email']);
        $userEntiy->setRole(new Role(UserRoleEnum::GUEST->value));

        $this->em->persist($userEntiy);
        $this->em->flush();
        return $userEntiy;
    }

    public function login(array $user): User {
        $user = $this->em->getRepository(User::class)->findOneBy([
            'username' => $user['username'],
            'password' => Auth::hashPassword($user['password']),
        ]);

        if (!$user) {
            throw new \Exception('User not found');
        }

        return Auth::login($user);
    }
}
