<?php

namespace AppSrc\Controllers\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class ApiController {

    public function getUser(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $id = $request->getAttribute('id');
        $response->getBody()->write((string)json_encode(['id' => $id]));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }
}
