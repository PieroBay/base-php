<?php

namespace AppSrc\Controllers\Api;

use Slim\Routing\RouteCollectorProxy;
use Slim\App;

class ApiRouting {

    public function __construct(App $app) {
        return $app->group('/api', function (RouteCollectorProxy $group) {
            return $group->get('/getuser/{id:[0-9]+}', '\AppSrc\Controllers\Api\ApiController:getUser');
        });
    }
}
