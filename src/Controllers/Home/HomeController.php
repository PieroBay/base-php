<?php

namespace AppSrc\Controllers\Home;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Core\BaseController;
use Slim\Routing\RouteContext;

class HomeController extends BaseController {

    public function home(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        return $this->render([
            'name' => 'word !'
        ], $request);
    }
}
