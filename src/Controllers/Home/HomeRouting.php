<?php

namespace AppSrc\Controllers\Home;

use Slim\Routing\RouteCollectorProxy;
use AppSrc\Enum\UserRoleEnum;
use App\Core\AccessGuardMiddleware;

class HomeRouting {

    public function __construct(RouteCollectorProxy $group) {
        $group->get('/olsd', '\AppSrc\Controllers\Home\HomeController:home')->setName('home');
        $group->get('/admin', '\AppSrc\Controllers\Home\HomeController:home')->add(new AccessGuardMiddleware($group, UserRoleEnum::ADMIN));
    }
}
